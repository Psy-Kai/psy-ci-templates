# Psy CI Templates

### [CodeChecker](https://github.com/Ericsson/codechecker)
Generate code quality reports with `CodeChecker`.
| name | description |
| - | - |
| `PSY_CODECHECKER_LOG_BUILD` | Set compile command to for [log](https://github.com/Ericsson/codechecker/blob/master/docs/analyzer/user_guide.md#log-) command |
| `PSY_CODECHECKER_LOGFILE` | Path to clang compilation database relative to `CI_PROJECT_DIR` |
| `PSY_CODECHECKER_SKIPFILE` | Path to [skipfile](https://github.com/Ericsson/codechecker/blob/master/docs/analyzer/user_guide.md#skip-file-) relative to `CI_PROJECT_DIR` |
| `PSY_CODECHECKER_CREATE_HTML_REPORT` | Set to `1` to create html reports |
```yml
codequality:
  extends:
    - .psy:codechecker:log
    - .psy:codechecker
  variables:
    PSY_CODECHECKER_SKIPFILE: CodeChecker.SKIPFILE
    PSY_CODECHECKER_CREATE_HTML_REPORT: 1
```

### [CppCheck](http://cppcheck.sourceforge.net/)
Generate code quality reports with `cppcheck`. The report is converted from xml to json with [`cppcheck-codequality`](https://gitlab.com/ahogen/cppcheck-codequality).  
| name | description |
| - | - |
| `PSY_CPPCHECK_SRC_DIR` | Source directory relative to `CI_PROJECT_DIR` |
| `PSY_CPPCHECK_CREATE_HTML_REPORT` | Set to `1` to create [html reports](http://cppcheck.sourceforge.net/manual.html#html-report) |
| `PSY_CPPCHECK_ADDITIONAL_ARGUMENT` | Use to set custom arguments |
```yml
codequality:
  extends: .psy:cppcheck
  variables:
    PSY_CPPCHECK_CREATE_HTML_REPORT: 1
```

### [gcovr](https://gcovr.com/en/stable/)
Generate test coverage reports with `gcovr`.
| name | description |
| - | - |
| `PSY_GCOVR_SRC_DIR` | Source directory relative to `CI_PROJECT_DIR` |
| `PSY_GCOVR_HTML_TITLE` | Define a [title](https://gcovr.com/en/stable/guide.html#cmdoption-gcovr-html-title) for the generate html report |
| `PSY_GCOVR_CONFIG` | Set path to to [config](https://gcovr.com/en/stable/guide.html#cmdoption-gcovr-config) file |
| `PSY_GCOVR_EXCLUDE_THROW_BRANCHES` | Set to `0` to disable [exclusion of throw branches](https://gcovr.com/en/stable/guide.html#cmdoption-gcovr-exclude-throw-branches) |
| `PSY_GCOVR_ADDITIONAL_ARGUMENTS` | Use to set custom arguments |
```yml
unittest:
  stage: test
  extends: .psy:gcovr
  script:
    # we assume that a makefile with tests were generated and tests are compiled with --coverage
    - make test
```

### [gtest-reports](https://github.com/google/googletest/blob/master/docs/advanced.md#generating-an-xml-report)
Generate gtest xml reports.
```yml
unittest:
  stage: test
  extends: .psy:gtest-reports
  script:
    # we assume that a makefile with gtests were generated
    - make test
```
